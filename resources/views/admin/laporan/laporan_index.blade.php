<div class="container">
    <h1>Daftar Laporan</h1>
    <table class="table">
        <thead>
            <tr>
                <th>ID Laporan</th>
                <th>ID Kegiatan</th>
                <th>Status Promosi</th>
                <th>Tanggal Laporan</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($laporan as $laporan)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $laporan->id_kegiatan }}</td>
                    <td>{{ $laporan->status_promosi }}</td>
                    <td>{{ $laporan->tanggal_laporan }}</td>
                    <td>
                        <a href="{{ route('admin.laporan.edit', $laporan->id) }}" class="btn btn-primary">Edit</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
