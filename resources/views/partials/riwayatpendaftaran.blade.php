<style>
    .data-row {
        display: grid;
        grid-template-columns: 1fr 3fr 3fr 3fr 2fr 1fr;
        align-items: center;
        border-top: 1px solid #ccc;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .data-row:first-child {
        border-top: none;
    }

    .data-cell {
        text-align: left;
    }

    .status-badge {
        margin-top: 5px;
    }

    .detail-button {
        margin-top: 5px;
    }

    .custom-icon-size {
        width: 13px;
        height: 13px;
    }


</style>

<section id="riwayatpendaftaran">
    <div class="container mt-5 mb-5">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center bg-white">
                <h5 class="mb-0 ">Riwayat Pendaftaran</h5>
                <span><a href="{{ url('/User/form') }}" class="btn btn-success px-5">Daftar</a></span>
            </div>
            <div class="card m-4">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Sekolah</th>
                            <th scope="col">Provinsi</th>
                            <th scope="col">Tanggal Pengajuan</th>
                            <th scope="col">Status</th>
                            <th scope="col">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($data->count() == 0)
                        <tr>
                            <td colspan="6" class="text-center p-5">Belum ada data yang terdaftar</td>
                        </tr>

                        @endif
                        @foreach ($data as $key => $kegiatan)
                        <tr>
                            <td class="pt-3 pb-3 px-3">{{ $key+1 }}</td>
                            <td class="pt-3 pb-3">{{ $kegiatan->sekolah }}</td>
                            <td class="pt-3 pb-3">{{ $kegiatan->provinsi->provinsi }}</td>
                            <td class="pt-3 pb-3">{{ \Carbon\Carbon::parse($kegiatan->tanggal_kegiatan)->formatLocalized('%A, %e %B %Y') }}</td>
                            <td class="pt-3 pb-3">
                                @if($kegiatan->status_promosi == 'Diterima')
                                <span class="badge bg-success px-3 p-2 w-100 ">{{ $kegiatan->status_promosi }}</span>
                                @elseif($kegiatan->status_promosi == 'Diproses')
                                <span class="badge bg-info px-3 p-2 w-100">{{ $kegiatan->status_promosi }}</span>
                                @elseif($kegiatan->status_promosi == 'Ditolak')
                                <span class="badge bg-danger px-3 p-2 w-100">{{ $kegiatan->status_promosi }}</span>
                                @elseif($kegiatan->status_promosi == 'Selesai')
                                <span class="badge bg-success px-3 p-2 w-100">{{ $kegiatan->status_promosi }}</span>
                                @endif
                            </td>
                            <td class="pt-3 pb-3">
                                @if ($kegiatan->status_promosi == 'Diterima')
                                    <span>
                                        <a href="{{ route('detailKegiatan', $kegiatan->id) }}" class="badge bg-primary px-3 p-2" style="text-decoration: none">
                                            <i data-feather="eye" class="custom-icon-size" style="margin-right: 10px;"></i>Lihat
                                        </a>
                                    </span>
                                @elseif ($kegiatan->status_promosi == 'Ditolak')
                                    <a style="cursor: pointer; text-decoration: none" class="badge bg-primary px-3 p-2" data-toggle="modal" data-target="#exampleModal{{ $kegiatan->id }}" style="text-decoration: none">
                                        <i data-feather="eye" class="custom-icon-size" style="margin-right: 10px;"></i>Lihat
                                    </a>

                                    <!-- Modal -->
                                    {{-- <div class="modal fade" id="exampleModal{{ $kegiatan->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Catatan Penolakan</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <!-- Konten modal -->
                                                    <p>{{ $kegiatan->catatan_promosi }}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="modal fade" id="exampleModal{{ $kegiatan->id }}"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg ">
                                          <div class="modal-content ">
                                            <div class="modal-body bg-light p-5">
                                              <div class="container">
                                                <div class="row">
                                                  <div class="col-2 d-flex align-items-center">
                                                    <img style="width: 75px; height: 75px;" src="{{ asset('img/denied.png') }}" alt="logo">
                                                  </div>
                                                  <div class="col-8">
                                                    <div class="row">
                                                      <div class="col-12">
                                                        <h3 class="fw-bold">Catatan Penolakan</h3>
                                                      </div>
                                                      <div class="col-12">
                                                        <h6 class="text-secondary">Pendaftaran anda ditolak dikarenakan:</h6>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="col-2 d-flex justify-content-end">
                                                    <button type="button" class="btn-close " data-bs-dismiss="modal" aria-label="Close"></button>
                                                  </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 pt-5">
                                                      <p class="form-control border border-secondary h-100 bg-light" rows="4" readonly>
                                                        {{ $kegiatan->catatan_promosi }}
                                                      </p>
                                                    </div>
                                                  </div>
                                              </div>
                                            </div>
                                            <div class="modal-footer bg-danger border-0"></div>
                                          </div>
                                        </div>
                                      </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>


            </div>

        </div>
    </div>
</section>




