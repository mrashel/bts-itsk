<nav class="navbar navbar-expand-lg navbar-light bg-light ">
    <div class="container pt-4 pb-4">
        <!-- Logo -->
        <a class="navbar-brand" href="/">
            <img src="{{ asset('img/logohitam.png') }}" alt="Logo" height="45">
        </a>

        <!-- Toggle button untuk mobile -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Menu -->
        <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#manfaat">Manfaat</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/#faq">Daftar Pertanyaan</a>
                </li>
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/User/view') }}">Status</a>
                    </li>
                @endauth
            </ul>

            <!-- Tombol Sign Up atau Gambar Profile User -->
            @guest
                <a href="{{ url('/signin') }}">
                    <button class="btn btn-dark px-5 my-2 my-sm-0" type="submit">Sign in</button>
                </a>
            @else
                <div class="d-flex">
                    <!-- Ganti dengan gambar profile user -->
                    <a class="btn btn-light mx-4" href="{{ route('notification.index') }}"><i data-feather="bell"></i></a>
                    <div class="dropdown">
                        <img src="{{ asset('avatars/' . (Auth::user()->avatar ?? 'default.png')) }}" alt="Profile Image"
                            id="profileImage" class="rounded-circle" height="45" width="45" data-toggle="dropdown">
                        <div class="dropdown-menu">

                            @if (Auth::user()->role == 'mahasiswa')
                                <a class="dropdown-item" href="{{ route('user.profile') }}"><i data-feather="user"></i>
                                    Profile</a>
                            @elseif (Auth::user()->role == 'dosen')
                                <a class="dropdown-item" href="{{ route('dosen.profile') }}"><i data-feather="user"></i>
                                    Profile</a>
                            @endif

                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button type="submit" class="dropdown-item"><i data-feather="log-out"></i> Logout</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endguest
        </div>
</nav>
</section>
